<?php

/**
 * @file
 * Local development environment configuration file.
 *
 * Use this file to specify local configuration settings that will be ignored by
 * Pantheon, such as database credentials.
 *
 * To activate this file, copy and rename it such that its path plus filename is
 * 'sites/default/settings.local.php'.
 *
 * @see https://pantheon.io/docs/settings-php/
 */

// Local development environment configuration.
if (!defined('PANTHEON_ENVIRONMENT')) {
  // Database.
  $databases['default']['default'] = array(
    'database' => 'DATABASE',
    'username' => 'USERNAME',
    'password' => 'PASSWORD',
    'host' => 'localhost',
    'driver' => 'mysql',
    'port' => 3306,
    'prefix' => '',
  );
}
