<?php

/**
 * Example settings.php snippet: Enable Redis caching
 *
 * Redis is an open-source, networked, in-memory, key-value data store that can
 * be used as a drop-in caching backend Drupal.
 *
 * Copy this example snippet into your settings.php file to enable the Redis
 * cache backend for your Pantheon site.
 *
 * Note: requires having Redis enabled for the Pantheon site (not available for
 * Personal level sites) and having the Redis contrib module installed and
 * enabled.
 *
 * @see https://pantheon.io/docs/redis/
 */
if (defined('PANTHEON_ENVIRONMENT')) {
  // Use Redis for caching.
  $conf['redis_client_interface'] = 'PhpRedis';

  // Point Drupal to the location of the Redis plugin.
  $conf['cache_backends'][] = 'profiles/ua_quicsktart/modules/contrib/redis/redis.autoload.inc';

  $conf['cache_default_class'] = 'Redis_Cache';
  $conf['cache_prefix'] = array('default' => 'pantheon-redis');

  // Do not use Redis for cache_form (no performance difference).
  $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';

  // Use Redis for Drupal locks (semaphore).
  $conf['lock_inc'] = 'profiles/ua_quicsktart/modules/contrib/redis/redis.lock.inc';
}
