# UA Quickstart Drupal 7 Upstream for Pantheon

## End of life (2023-11-15)

As of November 15, 2023, UA Quickstart (Quickstart 1) and all of its associated repositories will no longer supported by the Arizona Digital team.  Any website still using Quickstart 1 after the end-of-life date should be migrated to Quickstart 2 as soon as possible.

See the [main UA Quickstart repository](https://bitbucket.org/ua_drupal/ua_quickstart/src/7.x-1.x/README.md) for more information.

### Pantheon-specific end-of-life considerations

- Historically sites using this UA Quickstart upstream could rely on security updates for Drupal core and the contributed modules included in UA Quickstart being available to apply from the upstream. This will no longer be possible after the end-of-life date.
- Prior to the end-of-life date, the upstream inclduded `.drush-lock-update` files that prevented site developers from updating Drupal core and the contributed modules included in UA Quickstart with drush.  As of the end-of-life date these files have been removed so that site developers can update Drupal core and the contributed modules included with Quickstart with drush as needed.
- Changing a UA Quickstart site's upstream to the default Pantheon Drupal 7 upstream (`drops-7`) is NOT recommended because the UA Quickstart upstream installs Drupal in a `web` subdirectory and the default Pantheon Drupal 7 upstream does not.
- Although updates have been made to address all known PHP 8.0/8.1 compatibility issues in UA Quickstart and its dependencies the default PHP version for the UA Quickstart Pantheon upstream is currently 7.3.  Individual sites can upgrade to PHP 8.0 or PHP 8.1 via the [pantheon.yml file](https://docs.pantheon.io/pantheon-yml#php-version) if desired although testing is strongly encouraged before deploying PHP version changes to production.