<?php

/**
 * @file
 * Default site configuration file for Drupal.
 *
 * @see https://pantheon.io/docs/settings-php/
 */

/**
 * Include the upstream-specific settings file.
 *
 * Loads settings from the upstream-provided settings file. Removing this line
 * is discouraged.  Settings loaded from the upstream settings file can be
 * overridden in the "Site-specific settings" section of this file.
 */
include __DIR__ . "/settings.upstream.php";

/**
 * Site-specific settings
 *
 * Custom, site-specific settings and overrides for upstream settings should be
 * added here. Example snippets can also be copied from the example files
 * provided in the `settings-examples` folder of the upstream repository.
 */
/*******************************************************************************
 * BEGIN CUSTOM SETTINGS
 ******************************************************************************/


// Add custom settings/overrides here.


/*******************************************************************************
 * END CUSTOM SETTINGS
 ******************************************************************************/

/**
 * Local development environment settings
 *
 * Loads local settings file, if one exists.  Local development environment
 * specific  settings (e.g. local database server credentials) should be added
 * to a settings.local.php file to avoid inadvertently adding any sensitive
 * local configuration to source code repositories.
 *
 * @see settings-examples/example.settings.local.php
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}
