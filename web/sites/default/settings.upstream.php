<?php

/**
 * @file
 * Upstream configuration file for UA QuickStart sites.
 *
 * IMPORTANT:
 * Do not modify this file.  This file is maintained by the UA QuickStart
 * upstream maintainers.
 *
 * Site-specific modifications belong in settings.php, not this file. This file
 * may change in future releases and modifications would cause conflicts when
 * attempting to apply upstream updates.
 */

/**
 * Default configuration for all Pantheon Environment requests.
 *
 * Settings in this block will be loaded for ALL requests (web and CLI) to
 * Pantheon environments.
 */
if (defined('PANTHEON_ENVIRONMENT')) {
  /**
   * Redirect all http requests to a pantheonsite.io domain to https
   */
  if (php_sapi_name() !== 'cli') {
    if (
      preg_match('/.*\.pantheonsite\.io$/', $_SERVER['HTTP_HOST']) &&
      (!isset($_SERVER['HTTP_USER_AGENT_HTTPS'])
        || $_SERVER['HTTP_USER_AGENT_HTTPS'] !== 'ON')
    ) {
      header('HTTP/1.0 301 Moved Permanently');
      header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
      exit();
    }
  }

  /**
   * Configure Migrate module to always use Drush for import/rollback operations
   * to prevent timeout issues on Pantheon environments.
   *
   * @see https://pantheon.io/docs/timeouts/
   * @see https://www.drupal.org/node/1958170
   */
  $conf['migrate_drush_path'] = $_ENV['HOME'] . '/bin/drush';
  $conf['migrate_import_method'] = 1;

  /**
   * Override the 'cas_cert' path setting provided by the UA CAS module with the
   * Pantheon system CA bundle path.
   *
   * This is needed to prevent phpCAS errors after cloning a site database from
   * one environment to another (e.g. from live to dev).
   */
  $conf['cas_cert'] = '/etc/pki/tls/certs/ca-bundle.crt';

  /**
   * Configure alternate tmp path for certain modules.
   *
   * @see https://pantheon.io/docs/temp-files/#private-temporary-files
   */
  // Plupload integration module.
  $conf['plupload_temporary_uri'] = 'private://tmp';

  // Views data export module.
  // @see https://www.drupal.org/node/1782038
  $conf['views_data_export_directory'] = 'private://tmp';

  // Webform module.
  // @see https://www.drupal.org/node/2402663
  $conf['webform_export_path'] = 'private://tmp';

  /**
   * Default performance and caching settings.
   *
   * @see https://pantheon.io/docs/drupal-cache/
   */
  // Minimum cache lifetime - should be none except for very high traffic sites.
  $conf['cache_lifetime'] = 0;

  // Cached page compression - always off.
  $conf['page_compression'] = 0;

  // Performance settings for test and live environments.
  if (in_array($_ENV['PANTHEON_ENVIRONMENT'], array('test', 'live'))) {
    // Anonymous caching - enabled.
    $conf['cache'] = 1;

    // TODO: Enable block caching once we've done more testing.
    // Block caching - enabled.
//    $conf['block_cache'] = 1;

    // Expiration of cached pages - 6 hours.
    $conf['page_cache_maximum_age'] = 21600;

    // Aggregate and compress CSS files in Drupal - on.
    $conf['preprocess_css'] = 1;

    // Aggregate JavaScript files in Drupal - on.
    $conf['preprocess_js'] = 1;
  }

  // Performance settings for development environments, including multidev.
  else {
    // Anonymous caching - disabled.
    $conf['cache'] = 0;

    // Block caching - disabled.
    $conf['block_cache'] = 0;

    // Expiration of cached pages - none.
    $conf['page_cache_maximum_age'] = 0;

    // Aggregate and compress CSS files in Drupal - off.
    $conf['preprocess_css'] = 0;

    // Aggregate JavaScript files in Drupal - off.
    $conf['preprocess_js'] = 0;
  }

  // Pantheon advanced page cache and D8 cache settings.
  $conf['cache_backends'][] = 'profiles/ua_quickstart/modules/contrib/d8cache/d8cache-ac.cache.inc';
  $conf['cache_class_cache_views_data'] = 'D8CacheAttachmentsCollector';
  $conf['cache_class_cache_block'] = 'D8CacheAttachmentsCollector';
  $conf['cache_class_cache_page'] = 'D8Cache';

  /**
   * Pantheon-speciic settings for Environment Indicator module.
   *
   * Changes the color of the administrative toolbar based on the enviroment and
   * adds environment description:
   * - Dev: Sage background color, white text.
   * - Test: Mesa background color, white text.
   * - Live: Deafault navbar module colors.
   * - Multidev: Azurite background color, white text.
   *
   * @see https://pantheon.io/docs/environment-indicator/
   */
  switch ($_ENV['PANTHEON_ENVIRONMENT']) {
    case 'dev':
      $conf['environment_indicator_overwritten_name'] = 'Dev';
      $conf['environment_indicator_overwritten_color'] = '#4a634e';
      $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
      $conf['environment_indicator_remote_release'] = $_ENV['PANTHEON_DEPLOYMENT_IDENTIFIER'];
      break;

    case 'test':
      $conf['environment_indicator_overwritten_name'] = 'Test';
      $conf['environment_indicator_overwritten_color'] = '#a95c42';
      $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
      $conf['environment_indicator_remote_release'] = 'pantheon_' . $_ENV['PANTHEON_DEPLOYMENT_IDENTIFIER'];
      break;

    case 'live':
      $conf['environment_indicator_overwritten_name'] = 'LIVE';
      $conf['environment_indicator_overwritten_color'] = '#0f0f0f';
      $conf['environment_indicator_overwritten_text_color'] = '#dddddd';
      $conf['environment_indicator_remote_release'] = 'pantheon_' . $_ENV['PANTHEON_DEPLOYMENT_IDENTIFIER'];
      break;

    default:
      // Multidev catch-all.
      $conf['environment_indicator_overwritten_name'] = 'Multidev';
      $conf['environment_indicator_overwritten_color'] = '#1e5288';
      $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
      $conf['environment_indicator_remote_release'] = $_ENV['PANTHEON_ENVIRONMENT'];
      break;
  }
}
/**
 * Default configuration for non-Pantheon requests.
 *
 * Settings in this block will be loaded for ALL requests (web and CLI) to
 * non-Pantheon environments (e.g. local development environments).
 */
else {
  // Local settings for Environment Indicator module.
  $conf['environment_indicator_overwritten_name'] = 'Local';
  $conf['environment_indicator_overwritten_color'] = '#707070';
  $conf['environment_indicator_overwritten_text_color'] = '#ffffff';
}

// Default Environment Indicator settings for all environments.
$conf['environment_indicator_overwrite'] = TRUE;
$conf['environment_indicator_overwritten_position'] = 'top';
$conf['environment_indicator_overwritten_fixed'] = FALSE;

/**
 * Default fast 404 settings provided with Drupal core.
 *
 * With these setting enalbed, the page request process will return a fast 404
 * page for missing files if they match the regular expression set in
 * '404_fast_paths' and not '404_fast_paths_exclude' below. 404 errors will
 * simultaneously be logged in the Drupal system log.
 *
 * UAQS site owners wishing to speed up server response times even further for
 * requests matching these patterns can add the following line to the end of the
 * "custom settings" section of their settings.php file, which will allow the
 * fast 404 responses to be returned earlier in the page request process and
 * prevent the 404 errors from being logged in the Drupal system log:
 * @code
 * drupal_fast_404();
 * @endcode
 *
 * @see default.settings.php
 */
$conf['404_fast_paths_exclude'] = '/\\/(?:styles)|(?:system\\/files)\\//';
$conf['404_fast_paths'] = '/\\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$conf['404_fast_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';
